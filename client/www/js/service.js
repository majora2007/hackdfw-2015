//angular.module('starter', ['ionic'])
app.factory('WeatherAPI', ['$http', '$q', function($http, $q) {
	return {
		getWeatherData: function(lat, long) {
			var weatherData = {};
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: 'http://api.wunderground.com/api/78425fdab612c6dc/conditions/q/'+lat+','+long+'.json',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
		          var str = [];
		          for(var p in obj)
		            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		          return str.join("&");
		        }
		    })
		    .success(function(data) {
		    	deferred.resolve(weatherData);
		    }).error(function(data) {
		    	deferred.resolve(weatherData);
		    });

		    return deferred.promise;
		},
		getWeatherDataMocked: function() {
			//var deferred = $q.defer();
			var weatherData = {
		        "response": {
		        "version":"0.1",
		        "termsofService":"http://www.wunderground.com/weather/api/d/terms.html",
		        "features": {
		        "conditions": 1
		        }
		        }
		        , "current_observation": {
		          "image": {
		          "url":"http://icons.wxug.com/graphics/wu2/logo_130x80.png",
		          "title":"Weather Underground",
		          "link":"http://www.wunderground.com"
		          },
		          "display_location": {
		          "full":"Merced, CA",
		          "city":"Merced",
		          "state":"CA",
		          "state_name":"California",
		          "country":"US",
		          "country_iso3166":"US",
		          "zip":"95343",
		          "magic":"1",
		          "wmo":"99999",
		          "latitude":"37.300000",
		          "longitude":"-120.483300",
		          "elevation":"55.00000000"
		          },
		          "observation_location": {
		          "full":"Northeast Merced, Merced, California",
		          "city":"Northeast Merced, Merced",
		          "state":"California",
		          "country":"US",
		          "country_iso3166":"US",
		          "latitude":"37.316147",
		          "longitude":"-120.450493",
		          "elevation":"154 ft"
		          },
		          "estimated": {
		          },
		          "station_id":"KCAMERCE2",
		          "observation_time":"Last Updated on February 28, 9:36 PM PST",
		          "observation_time_rfc822":"Sat, 28 Feb 2015 21:36:26 -0800",
		          "observation_epoch":"1425188186",
		          "local_time_rfc822":"Sat, 28 Feb 2015 21:36:32 -0800",
		          "local_epoch":"1425188192",
		          "local_tz_short":"PST",
		          "local_tz_long":"America/Los_Angeles",
		          "local_tz_offset":"-0800",
		          "weather":"Clear",
		          "temperature_string":"46.3 F (7.9 C)",
		          "temp_f":46.3,
		          "temp_c":7.9,
		          "relative_humidity":"83%",
		          "wind_string":"Calm",
		          "wind_dir":"East",
		          "wind_degrees":90,
		          "wind_mph":0.0,
		          "wind_gust_mph":0,
		          "wind_kph":0,
		          "wind_gust_kph":0,
		          "pressure_mb":"1011",
		          "pressure_in":"29.87",
		          "pressure_trend":"+",
		          "dewpoint_string":"41 F (5 C)",
		          "dewpoint_f":41,
		          "dewpoint_c":5,
		          "heat_index_string":"NA",
		          "heat_index_f":"NA",
		          "heat_index_c":"NA",
		          "windchill_string":"46 F (8 C)",
		          "windchill_f":"46",
		          "windchill_c":"8",
		          "feelslike_string":"46 F (8 C)",
		          "feelslike_f":"46",
		          "feelslike_c":"8",
		          "visibility_mi":"10.0",
		          "visibility_km":"16.1",
		          "solarradiation":"--",
		          "UV":"0","precip_1hr_string":"0.00 in ( 0 mm)",
		          "precip_1hr_in":"0.00",
		          "precip_1hr_metric":" 0",
		          "precip_today_string":"0.15 in (4 mm)",
		          "precip_today_in":"0.15",
		          "precip_today_metric":"4",
		          "icon":"clear",
		          "icon_url":"http://icons.wxug.com/i/c/k/nt_clear.gif",
		          "forecast_url":"http://www.wunderground.com/US/CA/Merced.html",
		          "history_url":"http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=KCAMERCE2",
		          "ob_url":"http://www.wunderground.com/cgi-bin/findweather/getForecast?query=37.316147,-120.450493",
		          "nowcast":""
		        }
		      };
		      //deferred.resolve(weatherData);
		      return weatherData;

		}
	};
}]);