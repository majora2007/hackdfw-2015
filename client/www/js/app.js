// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.controller('MapController', ['$scope', '$ionicLoading', '$compile', 'WeatherAPI', 
  function($scope, $ionicLoading, $compile, WeatherAPI) {

    var SOCKET_URL = 'http://localhost:4000/';
    var bounds = new google.maps.LatLngBounds();

    /*
    var contentString = '<div class="marker-info" ng-click="clickTest()">' + 
                          '<h1>{{marker.title}}</h1>' +
                          '<div class="row">' + 
                            '<img src="img/temp.png" style="float:left;"></img>' + 
                            '<span>{{marker.weather.temp_f}}</span>' + 
                          '</div>' + 
                        '</div>';
    var compiled = $compile(contentString)($scope);
    */


    var contentString = function(title,temp_f,wind_mph,precip_inch){
      return '<div class="marker-info">'+
        '<h4>'+title+'</h4>'+
        '<div class="row">' +
          '<table>' +
            '<tr>'+
              '<td>' + '<img src="img/temp.png" style="float:left;"></img>' + '</td>'+
              '<td>' + '<span>'+temp_f+'</span>'+
            '</tr>'+
            '<tr>'+
              '<td>' + '<img src="img/wind.png" style="float:left;"></img>' + '</td>'+
              '<td>' + '<span>'+wind_mph+'</span>'+
            '</tr>'+
            '<tr>'+
              '<td>' + '<img src="img/percipitation.png" style="float:left;"></img>' + '</td>'+
              '<td>' + '<span>'+precip_inch+'</span>'+
            '</tr>'+
          '</table>'+
        '</div>'+
      '</div>';
    };

    /*
    var infoWindow = new google.maps.InfoWindow({
      content: contentString()
    });
    */

    var socket = io(SOCKET_URL);
    socket.on('connect',function() {
      console.log('CONNECTED TO GPS SERVER');
    });

    socket.on('gps_coord', function(coord){
      console.log(coord);
      var lat = coord.lat;
      var lng = coord.lng;
      var title = coord.tagTitle;

      var p = new google.maps.LatLng(lat, lng);
      var marker = createMarker(p, $scope.map,title);
      $scope.markers.push(marker);
      bounds.extend(p);
      $scope.map.fitBounds(bounds);

    }); 

  var markers = [
    {title: 'Dallas', latitude: 32.776664, longitude:-96.796988},
  ];

  $scope.markers = [];

  function createMarker(mPos, mMap, mTitle) {
    var m = new google.maps.Marker({
        position: mPos,//new google.maps.LatLng(mPos.coords.latitude, mPos.coords.longitude),
        map: mMap, 
        title: mTitle,
        animation: google.maps.Animation.DROP,
        infoWindow: new google.maps.InfoWindow()
      });

      var weather = WeatherAPI.getWeatherDataMocked().current_observation;

      var contentStr = contentString(mTitle,weather.temperature_string,
        weather.wind_string,weather.precip_today_string);
      var infowindow = new google.maps.InfoWindow({
        content: contentStr
      });

      google.maps.event.addListener(m, 'click', function(marker, i) {
        //$scope.marker = $scope.markers[i];
        //$scope.marker.infoWindow.setContent($compile(contentString)($scope)[0]);
        //$scope.marker.infoWindow.open($scope.map, $scope.marker);
        infowindow.open(mMap, m);
      });
    return m;
  }

  $scope.initialize = function() {
    var map;
    
    //var myLatlng = new google.maps.LatLng(37.3000, -120.4833);

    var mapOptions = {
      //center: myLatlng,
      zoom: 14, 
      mapTypeId: google.maps.MapTypeId.SATELLITE
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Custom marker info window
    for (i = 0; i < markers.length; i++) {
      var position = new google.maps.LatLng(markers[i].latitude, markers[i].longitude);
      bounds.extend(position);

      var marker = createMarker(position, map, markers[i].title);
      //loadMarkerData(marker);
      $scope.markers.push(marker);

      // Automatically center the map fitting all markers on the screen
      map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(16);
        google.maps.event.removeListener(boundsListener);
    });

    $scope.map = map;

    /*google.maps.event.addListener(marker, 'click', function() {
      infoWindow.open(map, marker);
    });*/

  };

  $scope.centerOnMe = function() {
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: true
    });

    navigator.geolocation.getCurrentPosition(function(pos) {
      map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function(error) {
      alert('Unable to get location: ' + error.message);
    });
  };

  $scope.clickTest = function() {
    alert('Example of infowindow with ng-click')
  };

  google.maps.event.addDomListener(window, 'load', $scope.initialize());

  /*
  function loadMarkerData(marker) {
    //WeatherAPI.getWeatherData($scope.marker.position.k, $scope.marker.position.D)
    var weather = WeatherAPI.getWeatherData().then(function(data) {
      console.log('weather data:');
      console.log(data);
    });

    marker.weather = weather.current_observation;
    console.log(marker);
  }
  */

}]);
